<?php
    class Animal {
        //Property
        protected $name,
               $legs,
               $cold_blooded;

        //method
        public function __construct($name, $legs=2, $cold_blooded=false){
            $this->name = $name;
            $this->legs = $legs;
            $this->cold_blooded = $cold_blooded;
            //$this->echoInit();
        }

        protected function echoInit(){
            echo "Constructed<br>";
            echo "Name : {$this->name}<br>";
            echo "Legs : {$this->legs}<br>";
            echo "Cold Blooded : ". ($this->cold_blooded ? 'true' : 'false') ."<br><br>";
        }

        public function get_name(){
            return $this->name;
        } 

        public function get_legs(){
            return $this->legs;
        } 

        public function get_cold_blooded(){
            return $this->cold_blooded ? 'true' : 'false';
        } 
    }
?>