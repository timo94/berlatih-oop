<?php
    class Ape extends Animal{
        public $type;

        public function __construct($name, $legs=2, $cold_blooded=false) {
            parent::__construct($name, $legs, $cold_blooded);
            $this->type = "Ape";
        }

        public function yell(){
            echo "Auoo";
        }
    }
?>