<?php
    class Frog extends Animal{
        public $type;

        public function __construct($name, $legs=4, $cold_blooded=true) {
            parent::__construct($name, $legs, $cold_blooded);
            $this->type = "Frog";
        }

        public function jump(){
            echo "hop hop";
        }
    }
?>